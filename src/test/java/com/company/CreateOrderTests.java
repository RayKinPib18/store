package com.company;

import org.apache.http.HttpResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class CreateOrderTests {

    @Test
    void OrderCreationWithId1() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(1, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(1, order.getId());
    }

    @Test
    void OrderCreationWithId0() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(0, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(0, order.getId());
    }

    @Test
    void OrderCreationWithNegativeId() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(-1, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(-1, order.getId());
    }

    @Test
    void OrderCreationWithId100() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(100, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(100, order.getId());
    }
    @Test
    void OrderCreationWithId100000000() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(100000000, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(100000000, order.getId());
    }

    @Test
    void OrderCreationWithQuantity0() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 0, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(0, order.getQuantity());
    }

    @Test
    void OrderCreationWithNegativeQuantity() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, -1, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(-1, order.getQuantity());
    }

    @Test
    void OrderCreationWithQuantity1000000000() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 1000000000, "2021-08-12T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(1000000000, order.getQuantity());
    }

    @Test
    void OrderCreationWithIncorrectDate() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 7, "18-08-2021T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(500, statusCode);
        Assertions.assertEquals("18-08-2021T07:06:18.643Z", order.getShipDate());
    }

    @Test
    void OrderCreationWithIncorrectDate2() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 7, "18.08.2021T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(500, statusCode);
        Assertions.assertEquals("18.08.2021T07:06:18.643Z", order.getShipDate());
    }

    @Test
    void OrderCreationWithIncorrectDate3() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 7, "T07:06:18.643Z", "approved", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println(response.getStatusLine());
        Assertions.assertEquals(500, statusCode);
        Assertions.assertEquals("T07:06:18.643Z", order.getShipDate());
    }

    @Test
    void OrderCreationWithStatusDelivered() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 23658, "2021-08-12T07:06:18.643Z", "delivered", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals("delivered", order.getStatus());
    }
    @Test
    void OrderCreationWithStatusAccepted() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 23658, "2021-08-12T07:06:18.643Z", "accepted", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals("accepted", order.getStatus());
    }

    @Test
    void OrderCreationWithBlankStatus() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 23658, "2021-08-12T07:06:18.643Z", "", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals("", order.getStatus());
    }

    @Test
    void OrderCreationWithNumericStatus() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 23658, "2021-08-12T07:06:18.643Z", "100", true);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals("100", order.getStatus());
    }

    @Test
    void OrderCreationWithCompleteFalse() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(2, 15268, 10, "2021-08-13T11:28:07.245Z", "approved", false);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertFalse(order.isComplete());
    }

    //значения "" и null не дает передавать компилятор. Тип данных для complete указан как boolean
    /*@Test
    void OrderCreationBlankComplete() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(2, 15268, 10, "2021-08-13T11:28:07.245Z", "approved", "");
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(2, order.getId());
        Assertions.assertEquals(15268, order.getPetId());
        Assertions.assertEquals(10, order.getQuantity());
        Assertions.assertEquals("2021-08-13T11:28:07.245Z", order.getShipDate());
        Assertions.assertEquals("approved", order.getStatus());
        Assertions.assertEquals(false, order.isComplete());
    }*/

    /*@Test
    void OrderCreationNullComplete() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(2, 15268, 10, "2021-08-13T11:28:07.245Z", "approved", null);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(2, order.getId());
        Assertions.assertEquals(15268, order.getPetId());
        Assertions.assertEquals(10, order.getQuantity());
        Assertions.assertEquals("2021-08-13T11:28:07.245Z", order.getShipDate());
        Assertions.assertEquals("approved", order.getStatus());
        Assertions.assertEquals(false, order.isComplete());
    }*/

    @Test
    void CreatingRecurringOrder() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(2, 15268, 10, "2021-08-13T11:28:07.245Z", "approved", true);
        storeClient.createOrderPOST(order);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
    }
}
