package com.company;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class FindOrderByIdTests {

    @Test
    void FindExistingOrderById() throws IOException {
        StoreClient storeClient = new StoreClient();
        Order order = new Order(2, 198772, 0, "2021-08-13T11:28:07.245Z", "approved", true);
        storeClient.createOrderPOST(order);
        HttpResponse response = storeClient.FindByIdGET(2);
        int statusCode = response.getStatusLine().getStatusCode();
        HttpEntity entity = response.getEntity();
        Assertions.assertEquals(200, statusCode);

        System.out.println(EntityUtils.toString(entity));
        System.out.println(response.getStatusLine());
    }

    @Test
    void FindNonExistingOrderById() throws IOException {
        StoreClient storeClient = new StoreClient();
        storeClient.DeleteById(10);
        HttpResponse response = storeClient.FindByIdGET(10);
        int statusCode = response.getStatusLine().getStatusCode();
        HttpEntity entity = response.getEntity();
        Assertions.assertEquals(404, statusCode);

        System.out.println(EntityUtils.toString(entity));
        System.out.println(response.getStatusLine());
    }

}
