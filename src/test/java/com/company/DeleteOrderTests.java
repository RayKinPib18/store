package com.company;

import org.apache.http.HttpResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class DeleteOrderTests {

    @Test
    void SuccessfulOrderCreationId1() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 198772, 7, "2021-08-12T07:06:18.643Z", "approved", true);
        storeClient.createOrderPOST(order);
        HttpResponse response = storeClient.DeleteById(10);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
    }

    @Test
    void DeletingNonExistentOrder() throws IOException {

        StoreClient storeClient = new StoreClient();
        storeClient.DeleteById(99);
        HttpResponse response = storeClient.DeleteById(99);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(404, statusCode);
    }
}
