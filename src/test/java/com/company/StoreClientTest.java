package com.company;

import org.apache.http.HttpResponse;
import org.junit.jupiter.api.*;

import java.io.IOException;

public class StoreClientTest {

    @Test
    void OrderCreation() throws IOException {

        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 10, 10, "2021-08-13T11:50:21.616Z", "placed", false);
        HttpResponse response = storeClient.createOrderPOST(order);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
        Assertions.assertEquals(10, order.getId());
        Assertions.assertEquals(10, order.getPetId());
        Assertions.assertEquals(10, order.getQuantity());
        Assertions.assertEquals("2021-08-13T11:50:21.616Z", order.getShipDate());
        Assertions.assertEquals("placed", order.getStatus());
        Assertions.assertFalse(order.isComplete());
    }

    @Test
    void InventoryCheck() throws IOException {
        StoreClient storeClient = new StoreClient();
        HttpResponse response = storeClient.InventoryGET();
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);

    }

    @Test
    void FindingById() throws IOException {
        StoreClient storeClient = new StoreClient();
        HttpResponse response = storeClient.FindByIdGET(10);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
    }

    @Test
    void DeleteById() throws IOException {
        StoreClient storeClient = new StoreClient();
        Order order = new Order(10, 10, 10, "2021-08-13T11:50:21.616Z", "placed", false);
        storeClient.createOrderPOST(order);
        HttpResponse response = storeClient.DeleteById(10);
        int statusCode = response.getStatusLine().getStatusCode();
        Assertions.assertEquals(200, statusCode);
    }
}
