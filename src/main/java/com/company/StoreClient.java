package com.company;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class StoreClient {


    private static final String Default_Url = "https://petstore.swagger.io/v2/store";
    private final ObjectMapper mapper = new ObjectMapper();


    public HttpResponse createOrderPOST(Order order) throws IOException {

        CloseableHttpClient httpClientCreate = HttpClients.createDefault();
        HttpPost postCreate = new HttpPost(Default_Url + "/order");
        StringEntity stringEntity = new StringEntity(mapper.writeValueAsString(order));
        postCreate.setEntity(stringEntity);
        postCreate.setHeader("Accept", "application/json");
        postCreate.setHeader("Content-type", "application/json");

        return httpClientCreate.execute(postCreate);
    }

    public HttpResponse InventoryGET() throws IOException {

        CloseableHttpClient httpClientGetInventory = HttpClients.createDefault();
        HttpGet getInventory = new HttpGet(Default_Url + "/inventory");

        return httpClientGetInventory.execute(getInventory);
    }

    public HttpResponse FindByIdGET(int id) throws IOException {
        CloseableHttpClient httpClientGetOrder = HttpClients.createDefault();
        HttpGet GetOrder = new HttpGet(Default_Url + "/order/" + id);

        return httpClientGetOrder.execute(GetOrder);
    }

    public HttpResponse DeleteById(int id) throws IOException {

        CloseableHttpClient httpClientDeleteOrder = HttpClients.createDefault();
        HttpDelete DeleteOrder = new HttpDelete(Default_Url + "/order/" + id);

        return httpClientDeleteOrder.execute(DeleteOrder);
    }

}
